import java.util.ArrayList;

public class MainClass {

	public static void main(String[] args) {
		System.out.println("��");
		System.out.println("======================================");
		double[] ab = {0, 2};
		int N = 20;
		double[] alpha = {0, 1};
		double[][] beta = {{0, 0}, {1, 0}};
		/*
		for(int i = 0; i < 2; i++) {
			for(int j = 0; j < 2; j++) {
				System.out.print(beta[i][j] + "\t");
			}				
			System.out.println();
		}
		*/
		double[] p = { 0.5 , 0.5};
		int q = 2;
		int n = 2;
		double h = 0.10;
		double[] ua = {Math.cos(0 * 0) * Math.sqrt(1 + 0),
				Math.sin(0 * 0) * Math.sqrt(1 + 0)};
		ua[0] = 1;
		ua[1] = 0;
		rk(ab,alpha, beta, p, q, n, N, ua);
		System.out.println("==========================================");
		for(int i = 0; i < toch_znch(ab, N).size(); i++) {
			System.out.println("t = " + toch_znch(ab, N).get(i)[2] + "\t" +toch_znch(ab, N).get(i)[0] + "\t" + toch_znch(ab, N).get(i)[1]);
		}
		
		
/*		
		for(int i = 0; i < rk(ab,alpha, beta, p, q, n, N, ua).size(); i++) {
			System.out.println("\t" +rk(ab,alpha, beta, p, q, n, N, ua).get(i)[0] + "\t" + rk(ab,alpha, beta, p, q, n, N, ua).get(i)[1]);
		}
*/		
		
	}
	
	
	/*
	 
	 q = 3
	 
	 p = 1/6	2/3		1/6
	 
	 a =
	 0	1/2	1
	 
	 b =
	 0		0	0
	 1/2	0	0
	 -1		2	0
	 
	 k1 = f(t,y)
	 k2 = f(t+h * 1/2, y+h*k1 * 1/2)
	 k3 = f(t+h * 1,   y+h*(k1 * -1 + k2* 2)
	 ===========================================
	 
	 q = 2
	 p = 1/2	1/2
	 a=
	 0	1
	 
	 b=
	 0	0
	 1	0
	 
	 k1 = f(t,y)
	 k2 = f(t+h * 1, y+h*k1 * 1)
	 
	 */
	
	private static ArrayList<double[]> rk(double[] ab, double[] alpha, double[][] beta,
			double[] p, int q, int n, int N, double[] ua) {//, double[] vect_curY) {
		//n = 2;
		ArrayList<double[]> Y = new ArrayList<>();
//		double[] vect_curY = new double[n];
		
		double[] vect_y = new double[n];
		vect_y = ua;
		//vect_y[0] = ua[0];
		//vect_y[1] = ua[1];
		//vect_y[0] = 0;
		//vect_y[1] = 1;//0.707106781186;
		double[] vect_temp = new double[n];
		//vect_temp = ua;
		//vect_temp[1] = 0.707106781186;
		//vect_temp[0] = 0;
		// vect_temp[1] = ua[1];
		// vect_temp[0] = ua[0];
		double t = ab[0];
		double[][] k = new double[q][n];
		double h = (ab[1] - ab[0]) / N;
		while (!Finish(t, ab[1])) {
			// /�������
			/*
			for (int i = 0; i < n; i++) {
				k[0][i] = f(t + h * alpha[0], vect_y)[i];
			}
			*/
			k[0] = f(t + h * alpha[0], vect_y);
			/*
			double[] sum = new double[n];
			for(int kol = 0 ; kol < 1; kol++) {
				 sum = ad(sum, mul(beta[1][kol], k[kol]));
			}
			vect_temp = ad(vect_y, mul(h, sum));
			k[1] = f(t + h * alpha[1], vect_temp);
			*/
			
			for (int j = 1; j < q; j++) {
				double[] sum = new double[n];
				for(int kol = 0 ; kol < j; kol++) {
					 sum = ad(sum, mul(beta[j][kol], k[kol]));
				}
				vect_temp = ad(vect_y, mul(h, sum));
				/*
				for (int i = 0; i < n; i++) {
					
					double sum = 0.0;
					for (int kol = 0; kol < j; kol++) {
						sum = sum + beta[j][kol] * k[kol][i];
					}
					vect_temp[i] = vect_y[i] + sum * h;
				}
				*/
				
				//vect_temp = ad(vect_y , mul(h , mul(beta[0][j], k[0])));
				/*
				for (int i = 0; i <= n - 1; i++) {
					k[j][i] = f(t + alpha[j] * h, vect_temp)[i];
				}
				*/
			
				k[j] = f(t + alpha[j] * h, vect_temp);
			}

			// ���� �� j ���������� ������� y
			/*
			for (int i = 0; i < n; i++) {
				vect_y[i] = vect_y[i] + h
						* (k[0][i] * p[0] + k[1][i] * p[1]);
			}
			*/
			vect_y = ad(vect_y, mul(h, ad(mul(p[0], k[0]), mul(p[1], k[1]))));
			System.out.println(r(t,2) + "\t" + vect_y[0] + "\t" + vect_y[1]);
			t = t + h;
			//vect_curY = vect_y;
			Y.add(vect_y);
		}

		return Y;
	}

	private static double[] f(double t, double[] vect_y) {
		double[] otvet = new double[2];
		otvet[0] = (vect_y[0] / (2.0 + 2.0 * t)) - (2.0 * t * vect_y[1]);
		otvet[1] = (vect_y[1] / (2.0 + 2.0 * t)) + (2.0 * t * vect_y[0]);
		/*
		otvet[0] = (-vect_y[1]) + vect_y[0]
				* (Math.pow(vect_y[0], 2) + Math.pow(vect_y[1], 2) - 1);
		otvet[1] = vect_y[0] + vect_y[1]
				* (vect_y[0] * vect_y[0] + vect_y[1] * vect_y[1] - 1);
		*/
		return otvet;
	}

	// ������
	private static ArrayList<double[]> toch_znch(double[]ab, int N) {
		ArrayList<double[]> Yt = new ArrayList<>();
		double h = (ab[1] - ab[0]) / N;
		double t = ab[0];
		for(int i = 0; i <= N; i++) {
			double[] otvet = new double[3];
			otvet[0] = Math.cos(t * t) * Math.sqrt(1 + t);
			otvet[1] = Math.sin(t * t) * Math.sqrt(1 + t);
			otvet[2] = r(t,2);
			t = t + h;
			Yt.add(otvet);
		}
		/*
		otvet[0] = Math.cos(t) / Math.sqrt((1 + Math.exp(2 * t)));
		otvet[1] = -Math.sin(t) / Math.sqrt((1 + Math.exp(2 * t)));
		*/
		return Yt;
	}

	private static boolean Finish(double t, double b) {
		return (t >= b);
	}

	private static double[] mul(double a, double[] b) {
		double[] c = new double[b.length];
		for(int i = 0; i < b.length; i++) {
			c[i] = b[i] * a;
		}
		return c;
	}
	
	private static double[] ad(double[] a, double[] b) {
		double[] c = new double[b.length];
		for(int i = 0; i < b.length; i++) {
			c[i] = b[i] + a[i];
		}
		return c;
	}
	
	private static double r(double value, int k){
        return (double)Math.round((Math.pow(10, k)*value))/Math.pow(10, k);
}
}
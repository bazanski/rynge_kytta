package using.chart;

import using.chart.RK_FOR_GUI;

import java.io.IOException;
import java.util.ArrayList;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class ScaterPlot {

	private static final long serialVersionUID = 1L;

	static RK_FOR_GUI rk;
	
	  public ScaterPlot(String applicationTitle, String plotTitle) throws IOException {
		  rk = new RK_FOR_GUI();
	       // super(applicationTitle);
	        // This will create the dataset 
	        XYDataset dataset = createDataset();
	        // based on the dataset we create the chart
	        JFreeChart chart = createChart(dataset, plotTitle);
	        
	     // create and display a frame...
	        ChartFrame frame = new ChartFrame("������", chart);
	        frame.pack();
	        frame.setVisible(true);

	    }
	    
	    
	/**
	     * Creates a sample dataset 
	     */
/*
	    private  XYDataset createDataset() {
	    	DefaultXYDataset result = new DefaultXYDataset();
	    	double[][] data = {{10,10},{20,20}};
	    	result.addSeries(null, data);
	    	
	        return result;
	        
	    }
	    */
	    //private static final Random r = new Random();

	    private static XYDataset createDataset() {
	        XYSeriesCollection result = new XYSeriesCollection();
	        XYSeries series = new XYSeries("");
	        
	        ArrayList<double[]> RK = new ArrayList<>();
	        try {
				RK = rk.getRK();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        for(int i = 0; i < RK.size(); i++) {
	        	double x = RK.get(i)[0];
	        	double y = RK.get(i)[1];
	        	series.add(x, y);
	        }
	        /*
	        for (int i = 0; i <= 100; i++) {
	      //      double x = r.nextDouble();
	      //      double y = r.nextDouble();
	      //      series.add(x, y);
	        }
	        */
	        result.addSeries(series);
	        return result;
	    }
	    
	    
	/**
	     * Creates a chart
	     */

	    private JFreeChart createChart(XYDataset dataset, String title) {
	        
	    	JFreeChart chart = ChartFactory.createScatterPlot(title, "R", "Z", dataset);
	    	
	    	/*
	        PiePlot3D plot = (PiePlot3D) chart.getPlot();
	        plot.setStartAngle(290);
	        plot.setDirection(Rotation.CLOCKWISE);
	        plot.setForegroundAlpha(0.5f);
	        */
	        return chart;
	        
	    }
}

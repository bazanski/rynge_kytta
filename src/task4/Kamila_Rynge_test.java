package task4;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.ArrayList;

//����� ������ rynge Kytta
public class Kamila_Rynge_test {

	final static double E = 2.718281828459045;
	final static double e1 = 0.0001;// * Math.pow(10, -330);
	final static double e2 = 0.01;
	
	static double[][] RK;
	static double[][] TOCH;
	
	static FileWriter os;
	static FileWriter excel;
	
	
	public static void main(String[] args) throws IOException {
		
		os = new FileWriter("Kamila_Task4.txt");
		excel = new FileWriter("Kamila_Task4.csv");
		
		
		//���������� ���������
		int n = 2;
		int N = 20000;
		//����� ����������
		
		//������� 5
		String f = "f5";
		String t = "toch_znch5";
		double[] ab = {0, 3};
		double[] alpha = {0, 0.5, 0.5, 1};
		double[][] beta = {{0, 0, 0, 0}
						, {0.5, 0, 0, 0}
						, { 0, 0.5, 0, 0}
						, {0, 0, 1, 0}};
		double[] p = { 1.0 / 6, 1.0 / 3, 1.0 / 3, 1.0 / 6};
		double[] ua = {2, 2};
		int q = 4;
			
		dinamic_rk_new_final(ab,alpha, beta, p, q, n, N, ua);
		

		os.close();
		excel.close();
	}
	
	private static ArrayList<double[]> dinamic_rk_new_final(double[] ab, double[] alpha, double[][] beta,
			double[] p, int q, int n, int N, double[] ua) throws IOException {

        ArrayList<double[]> allY = new ArrayList<double[]>(); 

        double[] y = ua.clone();
        double[] y0 = ua.clone();
        double[] y1 = ua.clone();
        double[] y2 = ua.clone();
        double t = ab[0],
    //���
        h = 1e-7;

        allY.add(ua.clone());

	    System.out.println("e \t y1 \t y2 \t h \t t");
	    os.write("e \t y1 \t y2 \t h \t t \n");
	    excel.write("e;y1;y2;h;t;\n");	    
	    
		System.out.println(0 + "\t" + y[0] + "\t" + y[1] + "\t" + h + "\t" + t);
		os.write(0 + "\t" + y[0] + "\t" + y[1] + "\t" + h + "\t" + t + "\n");
		excel.write(0 + ";" + y[0] + ";" + y[1] + ";" + h + ";" + t + ";\n");
		
	    while( t <= ab[1] ) {
	
	        y = one_step_final( t, h, y0, alpha, beta, p, q, n ).clone();
	        y1 = one_step_final( t, h/2, y0, alpha, beta, p, q, n ).clone();
	        y2 = one_step_final( t+h/2, h/2, y1, alpha, beta, p, q, n ).clone();
	
	        double[] oE = new double[n];
	        double e = 0;
	        int count_y = n, count_k = q;
	        for( int number_y = 0; number_y < count_y; number_y++ ) {
	            oE[number_y] = Math.abs((y1[number_y] - y2[number_y]) / (Math.pow(2, count_k - 1) - 1));
	            e += oE[number_y];
	        }
	
	        e = e/count_y;
	        if ( e < e1 ) {
	        	allY.add(y.clone());
	            y0 = y.clone();
	            t += h;
	            h = h * 2;
	        }
	
	        if ( e > e2 ) {
	            h = h / 2;
	        }
	
	        if( e1 <= e && e <= e2 ) {
	        	allY.add(y.clone());
	            y0 = y2.clone();
	            t += h;
	        }
	        System.out.println(e + "\t" + y[0] + "\t" + y[1] + "\t" + h + "\t" + t);
	        os.write(e + "\t" + y[0] + "\t" + y[1] + "\t" + h + "\t" + t + "\n");
	        excel.write(e + ";" + y[0] + ";" + y[1] + ";" + h + ";" + t + ";\n");
	    }
	    return allY;
	}

	private static double[] one_step_final(double t, double h, double[] start_y, double[] alpha, double[][] beta, double[] p, int q, int n) {
		double[][] array_k = new double[q][n];
		double[] yTemp = {0, 0},
		           y = start_y.clone();
		array_k[0][0] = 0;
		array_k[0][1] = 0;

		array_k[0] = f5_final( t + h*alpha[0], start_y);

		int count_k = q, count_y = n;
		for ( int number_k = 1; number_k < count_k; number_k++ ) {
	           //����������  ��������� y � k=f(t,y)
	           for ( int number_y = 0; number_y < count_y; number_y++ ) {
	               // ����( beta*k )
	               double sumBeta_K = 0;
	               for ( int j = 0; j < number_k; j++ ) {
	                   sumBeta_K += array_k[j][number_y]*beta[number_k][j];
	               };
	               yTemp[number_y] = start_y[number_y] + h * sumBeta_K;
	           };

	           array_k[number_k] = f5_final( t + h*alpha[number_k], yTemp ).clone();
	       };

	       double[] sumP_K = {0,0,0};
	       for ( int number_y = 0; number_y < count_y; number_y++ ) {
	           for ( int number_k = 0; number_k < count_k; number_k++ ) {
	               sumP_K[number_y] += p[number_k]*array_k[number_k][number_y];
	           };
	           y[number_y] += h*sumP_K[number_y];
	       };
	       return y;      
	}
	
	private static double[] f5_final(double t, double[] vect_y) {
		double nu = 0.2;
		double[] otvet = new double[2];
		otvet[0] = vect_y[1] - vect_y[0] * ( (vect_y[0] * vect_y[0] / 3) - 1 );//y1" 
		otvet[1] = -nu;//y2"
		
		return otvet;
	}
}
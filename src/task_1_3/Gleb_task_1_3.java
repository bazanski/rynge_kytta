package task_1_3;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.ArrayList;

//����� ������ rynge Kytta
public class Gleb_task_1_3 {

	final static double E = 2.718281828459045;
	final static double e1 = 0.0001;// * Math.pow(10, -330);
	final static double e2 = 0.01;
	
	static double[][] RK;
	static double[][] TOCH;
	static double Phi0;
	static FileOutputStream fileOutputStream = null;
	static BufferedWriter bufferedWriter = null;
	
	public static void main(String[] args) throws IOException {
		
		
		//���������� ���������
		int n = 2;
		int N = 20000;
		//����� ����������
		
		//������� 10 ��� ��������
		String f = "f10";
		String t = "toch_znch10";
		double[] ab = {0, 2};
		double[] alpha = {0, 1};
		double[][] beta = {{0, 0}
						, {1, 0}};
		double[] p = { 0.5 , 0.5};
		double[] ua = {1, 0};
		int q = 2;
		
		//===================================================
		
		//dinamic_rk_new(ab,alpha, beta, p, q, n, N, ua);
		
		RK = new double[N + 1][n];
		TOCH = new double[N + 1][n + 1];

		RK = rk(ab,alpha, beta, p, q, n, N, ua);
		TOCH = toch_znch10(ab, N, n);
		
		printResults(RK, TOCH); //����� ���������� ������ �� � ������� �������
		//printRazn(RK, TOCH); //����� ������� ����� ���������� �� � ������� ������� �� ������ ���� h
		
		int k = 100;
		//allMaxRazn(ab, alpha, beta, p, q, n, ua, N, f, t, k);// ����� ������� ����� ��������� �� � ������� ��� k ���������� ,
		//��� ������ ���������� � N ����� � ���������� ������ ����������� ��� +100 �����
		//�������� ���������� �� ������� printResults , printRazn , rk � toch_znch => ����� ���������� ��������
	}

	//�������� �������
	private static double[][] rk(double[] ab, double[] alpha, double[][] beta,
			double[] p, int q, int n, int N, double[] ua) {

		double[][] Y = new double[N+1][n];
		double[] vect_y = new double[n];
		vect_y = ua.clone();
		double[] vect_temp = new double[n];
		double t = ab[0];
		double[][] k = new double[q][n];
		double h = (ab[1] - ab[0]) / N;
		//h = 0.00000001;
		Y[0] = vect_y.clone();
		int Yi = 1;
		try {
			
			while (!isFinish(t, ab[1])) {
				
				//������� k1
				k[0] = f10(t + h * alpha[0], vect_y).clone();
							
				//������� ��� ���������� k �� q-�� k
				for (int j = 1; j < q; j++) {
					double[] sum = new double[n];
					for(int kol = 0 ; kol < j; kol++) {
						 sum = ad(sum, mul(beta[j][kol], k[kol]));
					}
					vect_temp = ad(vect_y, mul(h, sum));
					k[j] = f10(t + alpha[j] * h, vect_temp).clone();
				}
	
				// ������� Yn+1 � ������� Y � k1, ... , kq
				for (int i = 0; i < n; i++) {
					double[] sum = new double[n];
					for(int j = 0; j < q; j++) {
						sum[i] += k[j][i] * p[j];
					}
					vect_y[i] = vect_y[i] + h * sum[i];
				}
				
				t = t + h;
				Y[Yi] = vect_y.clone();
				Yi++;			}
		}
		catch(Exception x) {
		}
		return Y;
	}
	
	//������� ��� �������� ������� 10
	private static double[] f10(double t, double[] vect_y) {
		double[] otvet = new double[2];
		otvet[0] = (vect_y[0] / (2.0 + 2.0 * t)) - (2.0 * t * vect_y[1]);
		otvet[1] = (vect_y[1] / (2.0 + 2.0 * t)) + (2.0 * t * vect_y[0]);
		return otvet;
	}
	
	// ������ �������� ������� �������10 (��� ��������)
	private static double[][] toch_znch10(double[]ab, int N, int n) {
		double[][] Yt = new double[N+1][n+1];
		double h = (ab[1] - ab[0]) / N;
		double t = ab[0];
		for(int i = 0; i <= N; i++) {
			double[] otvet = new double[3];
			otvet[0] = Math.cos(t * t) * Math.sqrt(1 + t);
			otvet[1] = Math.sin(t * t) * Math.sqrt(1 + t);
			otvet[2] = r(t,2);
			t = t + h;
			Yt[i] = otvet;
			//System.out.println("t = " + Yt.get(i)[2] + "\t" +Yt.get(i)[0] + "\t" + Yt.get(i)[1]);
		}
		return Yt;
	}
	
	//�������� ��������� ������
	private static boolean isFinish(double t, double b) {
		return (t > b);
	}

	//��������������� �������
	//����� ����������� � �������
	private static void printResults(double[][] R, double[][] T) {
		//System.out.println("������ � = " + T.length);
		System.out.println("������ �� = " + R.length);
		System.out.println("======================================");
		//System.out.println("� \t �����E y1 \t\t �����-����� �1 \t �����E y2 \t\t �����-����� �2");
		System.out.println("======================================");
		for(int i = 0; i < R.length; i++){
			System.out.println(T[i][2] + "\t" + T[i][0] + "\t" + R[i][0] + "\t" + T[i][1] + "\t" + R[i][1]);
			//System.out.println(R[i][0] + "\t" + R[i][1] + "\t" + R[i][2]);
		}
		//System.out.println("� \t �����E y1 \t\t �����-����� �1 \t �����E y2 \t\t �����-����� �2");
		System.out.println("======================================");
		
	}
	
	//����� ������������ � �������
	private static void printRazn(double[][] R, double[][] T) {
		System.out.println("������ � = " + T.length);
		System.out.println("������ �� = " + R.length);
		System.out.println("======================================");
		System.out.println("�����������");
		System.out.println("� \t|�����E y1 - �����-����� �1| \t|�����E y2 - �����-����� �2|");
		for(int i = 0; i < T.length; i++){
			System.out.println(T[i][2] + "\t" + Math.abs(T[i][0] - R[i][0]) + "\t\t" + Math.abs(T[i][1] - R[i][1]));
		}
		System.out.println("� \t|�����E y1 - �����-����� �1| \t|�����E y2 - �����-����� �2|");
	}
	
	//���������� ������������ �����������
	private static double[] maxRazn(double[][] R, double[][] T, int n) {
		double[] raznMax = new double[n];
		for(int i = 0; i < T.length; i++)
			for(int j = 0; j < n; j++){
				if(raznMax[j] < Math.abs(T[i][j] - R[i][j])) {
					raznMax[j] = Math.abs(T[i][j] - R[i][j]);
				}
			}
		return raznMax;
	}
	
	//���������� ������������ ������������
	private static void allMaxRazn(double[] ab, double[] alpha, double[][] beta,
			double[] p, int q, int n,double[] ua, int startN,int k) {
		//int k = 100;
		int N = startN;

		double[][] R, T;
		R = new double[startN+1][n];
		T = new double[startN+1][n+1];
		
		for(int i = 0; i < k; i++) {
			R = rk(ab,alpha, beta, p, q, n, N, ua);
			
			T = toch_znch10(ab, N, n);
			String text = String.valueOf(N + "\t");
			for(int j = 0; j < n; j++){
				text += String.valueOf(maxRazn(R, T, n)[j]) + "\t";
			}
			System.out.println(text);
			N += 100;
		}
		
	}
	
	//����������
	private static double r(double value, int k){
        return (double)Math.round((Math.pow(10, k)*value))/Math.pow(10, k);
	}

	//�������� ��������� �� ������
	private static double[] mul(double a, double[] b) {
		double[] c = new double[b.length];
		for(int i = 0; i < b.length; i++) {
			c[i] = b[i] * a;
		}
		return c;
	}
	
	//�������� ��������
	private static double[] ad(double[] a, double[] b) {
		double[] c = new double[b.length];
		for(int i = 0; i < b.length; i++) {
			c[i] = b[i] + a[i];
		}
		return c;
	}
	
}

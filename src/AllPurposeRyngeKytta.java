import java.lang.reflect.Method;
import java.util.ArrayList;


//����� ������ rynge Kytta
public class AllPurposeRyngeKytta {

	final static double E = 2.718281828459045;
	
	public static void main(String[] args) {
		System.out.println("����� ������. ��");
		System.out.println("======================================");
		
		Method[] allMethods = AllPurposeRyngeKytta.class.getDeclaredMethods();
		
		//���������� ���������
		int n = 2;
		int N = 20;
		//����� ����������
		
		//������� 10
		/*
		double[] ab = {0, 2};
		double[] alpha = {0, 1};
		double[][] beta = {{0, 0}, {1, 0}};
		double[] p = { 0.5 , 0.5};
		double[] ua = {Math.cos(0 * 0) * Math.sqrt(1 + 0),
				Math.sin(0 * 0) * Math.sqrt(1 + 0)};
		int q = 2;
		*/
		//������� 2
		double[] ab = {0, 5};
		double[] alpha = {0, 0.5, 1};
		double[][] beta = {{0, 0, 0}, {0.5, 0, 0}, {-1, 2, 0}};
		double[] p = { 1.0 / 6 , 2.0 / 3, 1.0 / 6};
		double[] ua = {Math.cos(0) / Math.pow(1 + Math.pow(E, 2 * 0), 0.5),
				Math.sin(0) / Math.pow(1 + Math.pow(E, 2 * 0), 0.5)};
		int q = 3;
		//������� 5
		/*
		double[] ab = {0, 3};
		double[] alpha = {0, 0.5, 0.5, 1};
		double[][] beta = {{0, 0, 0, 0}, {0.5, 0, 0, 0}, { 0, 0.5, 0, 0}, {0, 0, 1, 0}};
		double[] p = { 1.0 / 6, 1.0 / 3, 1.0 / 3, 1.0 / 6};
		double[] ua = {3 * 0 * 0 - 0 - 1 + Math.cos(0) + Math.sin(0),
				0 * 0 + 2 - Math.cos(0) + Math.sin(0)};
		int q = 4;
		*/
		//===================================================
		/*
		for (Method m : allMethods)
		    if (m.getName().equals("f2")) {
		    	rk(ab,alpha, beta, p, q, n, N, ua, m);
		    }
		System.out.println("======================================");
		System.out.println("����� ��������");
		toch_znch2(ab, N);
		*/
		maxRaz(ab,alpha, beta, p, q, n, ua);
	}
	//�������� �������
	private static ArrayList<double[]> maxRaz(double[] ab, double[] alpha, double[][] beta,
			double[] p, int q, int n, double[] ua) {
		ArrayList<double[]> R = new ArrayList<>();
		ArrayList<double[]> Y = new ArrayList<>();
		ArrayList<double[]> Yt = new ArrayList<>();
		Method[] allMethods = AllPurposeRyngeKytta.class.getDeclaredMethods();
		int N = 10;
		double[] maxRaz = {0, 0};
		for(int i = 1; i <= 100 ; i++ ) {
			for (Method m : allMethods)
			    if (m.getName().equals("f10")) {
			    	Y = rk(ab,alpha, beta, p, q, n, N, ua, m);
			    }
			Yt = toch_znch10(ab, N);
			for(int j = 0; j < Yt.size(); j ++) {
				if(maxRaz[0] < Abs(Y.get(j)[0] - Yt.get(j)[0])) {
					maxRaz[0] = Abs(Y.get(j)[0] - Yt.get(j)[0]);
				}
				if(maxRaz[1] < Abs(Y.get(j)[1] - Yt.get(j)[1])) {
					maxRaz[1] = Abs(Y.get(j)[1] - Yt.get(j)[1]);
				}
			}
			double[] r = {N,maxRaz[0], maxRaz[1]};
			//System.out.println("N = " + N + "\t r1max = " + maxRaz[0] + "\t r2max = " + maxRaz[1]);
			System.out.println(N + "\t" + maxRaz[0] + "\t" + maxRaz[1]);
			R.add(r);
			N += 10;
			maxRaz[0] = 0;
			maxRaz[1] = 0;
		}
		return R;
	}
	
	private static ArrayList<double[]> rk(double[] ab, double[] alpha, double[][] beta,
			double[] p, int q, int n, int N, double[] ua, Method m) {

		ArrayList<double[]> Y = new ArrayList<>();
		double[] vect_y = new double[n];
		vect_y = ua;
		double[] vect_temp = new double[n];
		double t = ab[0];
		double[][] k = new double[q][n];
		double h = (ab[1] - ab[0]) / N;
		Y.add(vect_y);
		//System.out.println(r(t,2) + "\t" + vect_y[0] + "\t" + vect_y[1]);
		try {
			Class c = m.getDeclaringClass();
		    Object ob = c.newInstance();
		    m.setAccessible(true);
		    
			while (!isFinish(t, ab[1])) {
				
				//������� k1
				k[0] = (double[]) m.invoke(ob, t + h * alpha[0], vect_y);
							
				//������� ��� ���������� k �� q-�� k
				for (int j = 1; j < q; j++) {
					double[] sum = new double[n];
					for(int kol = 0 ; kol < j; kol++) {
						 sum = ad(sum, mul(beta[j][kol], k[kol]));
					}
					vect_temp = ad(vect_y, mul(h, sum));
					k[j] = (double[]) m.invoke(ob, t + alpha[j] * h, vect_temp);
				}
	
				// ������� Yn+1 � ������� Y � k1, ... , kq
				for (int i = 0; i < n; i++) {
					double[] sum = new double[n];
					for(int j = 0; j < q; j++) {
						sum[i] += k[j][i] * p[j];
					}
					vect_y[i] = vect_y[i] + h * sum[i];
				}
				//vect_y = ad(vect_y, mul(h, ad(mul(p[0], k[0]), mul(p[1], k[1]))));
				
				t = t + h;
				//System.out.println(r(t,2) + "\t" + vect_y[0] + "\t" + vect_y[1]);
				Y.add(vect_y);
			}
		}
		catch(Exception x) {
			System.out.print( x.getMessage() );
		}
		return Y;
	}
	
	private static ArrayList<double[]> rk_dinamic_h(double[] ab, double[] alpha, double[][] beta,
			double[] p, int q, int n, int N, double[] ua, Method m) {

		ArrayList<double[]> Y = new ArrayList<>();
		ArrayList<double[]> Y1 = new ArrayList<>();
		ArrayList<double[]> Y2 = new ArrayList<>();
		double[] vect_y = new double[n];
		double[] vect_y1 = new double[n];
		double[] vect_y2 = new double[n];
		vect_y = ua;
		vect_y1 = ua;
		vect_y2 = ua;
		
		double[] vect_temp = new double[n];
		double t = ab[0];
		double[][] k = new double[q][n];
		double h = (ab[1] - ab[0]) / N;
		double din_h = h;
		Y1.add(vect_y1);
		Y2.add(vect_y2);
		System.out.println(r(t,2) + "\t" + vect_y[0] + "\t" + vect_y[1]);
		try {
			Class c = m.getDeclaringClass();
		    Object ob = c.newInstance();
		    m.setAccessible(true);
		    
			while (!isFinish(t, ab[1])) {
				
				//������� k1
				k[0] = (double[]) m.invoke(ob, t + din_h * alpha[0], vect_y1);
							
				//������� ��� ���������� k �� q-�� k
				for (int j = 1; j < q; j++) {
					double[] sum = new double[n];
					for(int kol = 0 ; kol < j; kol++) {
						 sum = ad(sum, mul(beta[j][kol], k[kol]));
					}
					vect_temp = ad(vect_y1, mul(din_h, sum));
					k[j] = (double[]) m.invoke(ob, t + alpha[j] * din_h, vect_temp);
				}
	
				// ������� Yn+1 � ������� Y � k1, ... , kq
				for (int i = 0; i < n; i++) {
					double[] sum = new double[n];
					for(int j = 0; j < q; j++) {
						sum[i] += k[j][i] * p[j];
					}
					vect_y1[i] = vect_y1[i] + din_h * sum[i];
				}
				//vect_y = ad(vect_y, mul(h, ad(mul(p[0], k[0]), mul(p[1], k[1]))));
				
				t = t + din_h;
				//============================
				//������� k1
				k[0] = (double[]) m.invoke(ob, t + din_h/2 * alpha[0], vect_y1);
							
				//������� ��� ���������� k �� q-�� k
				for (int j = 1; j < q; j++) {
					double[] sum = new double[n];
					for(int kol = 0 ; kol < j; kol++) {
						 sum = ad(sum, mul(beta[j][kol], k[kol]));
					}
					vect_temp = ad(vect_y1, mul(din_h/2, sum));
					k[j] = (double[]) m.invoke(ob, t + alpha[j] * din_h/2, vect_temp);
				}
	
				// ������� Yn+1 � ������� Y � k1, ... , kq
				for (int i = 0; i < n; i++) {
					double[] sum = new double[n];
					for(int j = 0; j < q; j++) {
						sum[i] += k[j][i] * p[j];
					}
					vect_y1[i] = vect_y1[i] + din_h/2 * sum[i];
				}
				//vect_y = ad(vect_y, mul(h, ad(mul(p[0], k[0]), mul(p[1], k[1]))));
				
				t = t + din_h/2;
				
				k[0] = (double[]) m.invoke(ob, t + din_h/2 * alpha[0], vect_y1);
				
				//������� ��� ���������� k �� q-�� k
				for (int j = 1; j < q; j++) {
					double[] sum = new double[n];
					for(int kol = 0 ; kol < j; kol++) {
						 sum = ad(sum, mul(beta[j][kol], k[kol]));
					}
					vect_temp = ad(vect_y1, mul(din_h/2, sum));
					k[j] = (double[]) m.invoke(ob, t + alpha[j] * din_h/2, vect_temp);
				}
	
				// ������� Yn+1 � ������� Y � k1, ... , kq
				for (int i = 0; i < n; i++) {
					double[] sum = new double[n];
					for(int j = 0; j < q; j++) {
						sum[i] += k[j][i] * p[j];
					}
					vect_y1[i] = vect_y1[i] + din_h/2 * sum[i];
				}
				//vect_y = ad(vect_y, mul(h, ad(mul(p[0], k[0]), mul(p[1], k[1]))));
				
				t = t + din_h/2;
				
				//y1, y2
			}
		}
		catch(Exception x) {
			System.out.print( x.getMessage() );
		}
		return Y;
	}
	
	
	//������� ��� �������� ������� 10
	private static double[] f10(double t, double[] vect_y) {
		double[] otvet = new double[2];
		otvet[0] = (vect_y[0] / (2.0 + 2.0 * t)) - (2.0 * t * vect_y[1]);
		otvet[1] = (vect_y[1] / (2.0 + 2.0 * t)) + (2.0 * t * vect_y[0]);
		return otvet;
	}
	
	//������� ������� 2
	private static double[] f2(double t, double[] vect_y) {
		double[] otvet = new double[2];
		otvet[0] = - Math.sin(t) / Math.pow((1 + Math.pow(E, 2 * t)) , 0.5) + vect_y[0] * (vect_y[0] * vect_y[0] + vect_y[1] * vect_y[1] - 1);
		otvet[1] = Math.cos(t) / Math.pow((1 + Math.pow(E, 2 * t)) , 0.5) + vect_y[1] * (vect_y[0] * vect_y[0] + vect_y[1] * vect_y[1] - 1);
		return otvet;
	}
	
	//������� ������� 5
	private static double[] f5(double t, double[] vect_y) {
		double[] otvet = new double[2];
		otvet[0] = vect_y[1] + t * t + 6 * t + 1; 
		otvet[1] = vect_y[0] - 3 * t * t + 3 * t + 1;
		return otvet;
	}
	
	
	// ������ �������� ������� �������10 (��� ��������)
	private static ArrayList<double[]> toch_znch10(double[]ab, int N) {
		ArrayList<double[]> Yt = new ArrayList<>();
		double h = (ab[1] - ab[0]) / N;
		double t = ab[0];
		for(int i = 0; i <= N; i++) {
			double[] otvet = new double[3];
			otvet[0] = Math.cos(t * t) * Math.sqrt(1 + t);
			otvet[1] = Math.sin(t * t) * Math.sqrt(1 + t);
			otvet[2] = r(t,2);
			t = t + h;
			Yt.add(otvet);
			//System.out.println("t = " + Yt.get(i)[2] + "\t" +Yt.get(i)[0] + "\t" + Yt.get(i)[1]);
		}
		return Yt;
	}
	
	// ������ �������� ������� �������2 (��� ��������)
	private static ArrayList<double[]> toch_znch2(double[]ab, int N) {
		ArrayList<double[]> Yt = new ArrayList<>();
		double h = (ab[1] - ab[0]) / N;
		double t = ab[0];
		for(int i = 0; i <= N; i++) {
			double[] otvet = new double[3];
			otvet[0] = Math.cos(t) / Math.pow(1 + Math.pow(E, 2 * t), 0.5);
			otvet[1] = Math.sin(t) / Math.pow(1 + Math.pow(E, 2 * t), 0.5);
			otvet[2] = r(t,2);
			t = t + h;
			Yt.add(otvet);
			System.out.println("t = " + Yt.get(i)[2] + "\t" +Yt.get(i)[0] + "\t" + Yt.get(i)[1]);
		}
		return Yt;
	}
	
	// ������ �������� ������� �������5 (��� ��������)
	private static ArrayList<double[]> toch_znch5(double[]ab, int N) {
		ArrayList<double[]> Yt = new ArrayList<>();
		double h = (ab[1] - ab[0]) / N;
		double t = ab[0];
		for(int i = 0; i <= N; i++) {
			double[] otvet = new double[3];
			otvet[0] = 3 * t * t - t - 1 + Math.cos(t) + Math.sin(t);
			otvet[1] = t * t + 2 - Math.cos(t) + Math.sin(t);
			otvet[2] = r(t,2);
			t = t + h;
			Yt.add(otvet);
			System.out.println("t = " + Yt.get(i)[2] + "\t" +Yt.get(i)[0] + "\t" + Yt.get(i)[1]);
		}
		return Yt;
	}
	
	
	//�������� ��������� ������
	private static boolean isFinish(double t, double b) {
		return (t >= b);
	}

	//��������������� �������
	//����������
	private static double r(double value, int k){
        return (double)Math.round((Math.pow(10, k)*value))/Math.pow(10, k);
	}

	//�������� ��������� �� ������
	private static double[] mul(double a, double[] b) {
		double[] c = new double[b.length];
		for(int i = 0; i < b.length; i++) {
			c[i] = b[i] * a;
		}
		return c;
	}
	
	//�������� ��������
	private static double[] ad(double[] a, double[] b) {
		double[] c = new double[b.length];
		for(int i = 0; i < b.length; i++) {
			c[i] = b[i] + a[i];
		}
		return c;
	}
	
	private static double Abs(double x) {
    	return x>0 ? x : -x;
    }
}

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;

//����� ������ rynge Kytta
public class Rynge {

	final static double E = 2.718281828459045;
	final static double e1 = 0.00001;
	final static double e2 = 0.001;
	
	static double[][] RK;
	static double[][] TOCH;
	
	static FileOutputStream out = null;
    
	static FileOutputStream fileOutputStream = null;
	static BufferedWriter bufferedWriter = null;
	
	public static void main(String[] args) throws IOException {
		
		
		try {
			fileOutputStream = new FileOutputStream("answer.txt");
			bufferedWriter = new BufferedWriter(
	                new OutputStreamWriter(fileOutputStream, "UTF-8"));
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//pathOfFolder + File.separatorChar +
							
		
		
		
		Method[] allMethods = Rynge.class.getDeclaredMethods();
		
		//���������� ���������
		int n = 2;
		int N = 30000;
		//����� ����������
		
		//������� 5
		/*
		String f = "f5";
		String t = "toch_znch5";
		double[] ab = {0, 3};
		double[] alpha = {0, 0.5, 0.5, 1};
		double[][] beta = {{0, 0, 0, 0}
						, {0.5, 0, 0, 0}
						, { 0, 0.5, 0, 0}
						, {0, 0, 1, 0}};
		double[] p = { 1.0 / 6, 1.0 / 3, 1.0 / 3, 1.0 / 6};
		double[] ua = {0, 1};
		int q = 4;
*/
		/*
		//������� 10 ��� ��������
		String f = "f10";
		String t = "toch_znch10";
		double[] ab = {0, 2};
		double[] alpha = {0, 1};
		double[][] beta = {{0, 0}
						, {1, 0}};
		double[] p = { 0.5 , 0.5};
		double[] ua = {1, 0};
		int q = 2;
		*/
		//������� 10 ��� ��������
		String f = "myFunction";
		String t = "toch_znch10";
		//double[] ab = {0, 2};
		double[] alpha = {0, 1};
		double[][] beta = {{0, 0}
						, {1, 0}};
		double[] p = { 0.5 , 0.5};
		double R0 = 0.001;
		double Z0 = 0.01;
		double Phi0 = 0.8;
		double[] ua = {R0, -Z0, Phi0};
		//double[] ab = {0, 20};
		double[] ab = {Phi0, Math.PI - Phi0};
		int q = 2;
		n = 3;
		//===================================================
		
		RK = new double[N+1][n];
		TOCH = new double[N+1][n+1];
		for (Method m : allMethods)
		    if (m.getName().equals(f)) {
		    	RK = rk(ab,alpha, beta, p, q, n, N, ua, m);
		    	//rk �����-����� ��� ������ ����������		    	
		    	//rk_dinamic_h �����-����� ��� ������������� ���� h 
		    	//����� ���������� �������� ��� ���� ���������� ������������� � ���������, ���������� ��������������� ������� printResults � printRazn � AllMaxRazn

		    }
		
		for (Method m : allMethods)
		    if (m.getName().equals(t)) {
		//    	TOCH = toch_znch(ab, N, n, m);
		    }
		
		printResults(RK, TOCH, Phi0, N); //����� ���������� ������ �� � ������� �������
		//printRazn(RK, TOCH); //����� ������� ����� ���������� �� � ������� ������� �� ������ ���� h
		
		int k = 100;
		//allMaxRazn(ab, alpha, beta, p, q, n, ua, N, f, t, k);// ����� ������� ����� ��������� �� � ������� ��� k ���������� ,
		//��� ������ ���������� � N ����� � ���������� ������ ����������� ��� +100 �����
		//�������� ���������� �� ������� printResults , printRazn , rk � toch_znch => ����� ���������� ��������
	}
	
	private static double[] myFunction(double t, double[] vect_y) {
		double alpha_sqr = 0.39;
		double r = vect_y[0], Z = vect_y[1], phi = vect_y[2];
		double[] otvet = new double[3];
		otvet[0] = Math.cos(phi);						// r"
		otvet[1] = -Math.sin(phi);						// Z"
		otvet[2] = (-Math.sin(phi) / r) - (Z / alpha_sqr);//phi"
		
		return otvet;
	}
	
	//�������� �������
	private static double[][] rk(double[] ab, double[] alpha, double[][] beta,
			double[] p, int q, int n, int N, double[] ua, Method m) {

		double[][] Y = new double[N+1][n];
		double[] vect_y = new double[n];
		vect_y = ua.clone();
		double[] vect_temp = new double[n];
		double t = ab[0];
		double[][] k = new double[q][n];
		double h = (ab[1] - ab[0]) / N;
		//h = 0.00000001;
		Y[0] = vect_y.clone();
		int Yi = 1;
		try {
			Class c = m.getDeclaringClass();
		    Object ob = c.newInstance();
		    m.setAccessible(true);
		    
			while (!isFinish(t, ab[1])) {
				
				//������� k1
				k[0] = ((double[]) m.invoke(ob, t + h * alpha[0], vect_y)).clone();
							
				//������� ��� ���������� k �� q-�� k
				for (int j = 1; j < q; j++) {
					double[] sum = new double[n];
					for(int kol = 0 ; kol < j; kol++) {
						 sum = ad(sum, mul(beta[j][kol], k[kol]));
					}
					vect_temp = ad(vect_y, mul(h, sum));
					k[j] = ((double[]) m.invoke(ob, t + alpha[j] * h, vect_temp)).clone();
				}
	
				// ������� Yn+1 � ������� Y � k1, ... , kq
				for (int i = 0; i < n; i++) {
					double[] sum = new double[n];
					for(int j = 0; j < q; j++) {
						sum[i] += k[j][i] * p[j];
					}
					vect_y[i] = vect_y[i] + h * sum[i];
				}
				
				t = t + h;
				Y[Yi] = vect_y.clone();
				Yi++;			}
		}
		catch(Exception x) {
		}
		return Y;
	}
	
	private static double[][] rk_dinamic_h(double[] ab, double[] alpha, double[][] beta,
			double[] p, int q, int n, int N, double[] ua, Method m) {
		int kol_e = 1;
		double[][] Y = new double[N+1][n];
		double[] vect_y = new double[n];
		double[] vect_y1 = new double[n];
		double[] vect_y2 = new double[n];
		double[] vect_y1old = new double[n];
		double[] vect_y2old = new double[n];
		vect_y = ua.clone();
		vect_y1 = ua.clone();
		vect_y2 = ua.clone();
		double[] vect_temp = new double[n];
		double t = ab[0];
		double t1 = ab[0];
		double t2 = ab[0];
		double[][] k1 = new double[q][n];
		double[][] k2 = new double[q][n];
		double h = (ab[1] - ab[0]) / N;
		Y[0] = vect_y.clone();
		int Yi = 1;
		try {
			Class c = m.getDeclaringClass();
		    Object ob = c.newInstance();
		    m.setAccessible(true);
		    
			while (!isFinish(t, ab[1])) {
				
				
				vect_y1 = rk_one_step(t, h, vect_y, alpha, beta, p, q, n, m).clone();
				//t1 = t1 + h;
				
				vect_y2 = rk_one_step(t2, h/2, vect_y, alpha, beta, p, q, n, m).clone();
				t2 = t2 + h/2;
				vect_y2 = rk_one_step(t2, h/2, vect_y2, alpha, beta, p, q, n, m).clone();
				t2 = t2 + h/2;
				//=====���������� �=======
				//2^q-1 - 1
				// e1 <= e <= e2
				double[] oE = new double[n];
				for(int i = 0; i < n; i++) {
					oE[i] = (vect_y1[i] - vect_y2[i]) / (Math.pow(2, q - 1) - 1);
				}
				double e = 0;
				for(int i = 0; i < n; i++) {
					e = e + oE[i];
				}
				e = Math.abs(e/n);
				System.out.println("=========");
				System.out.println(kol_e + " ");
				//t += h;
				System.out.println("h = " + h + " ");
				System.out.println("t = " + t + " ");
				System.out.println("e = " + e);
				
				
				if(e < e1) {
					System.out.println("e < e1");
					vect_y = vect_y2.clone();
					Y[Yi] = vect_y.clone();
					h = h * 2;
					t = t + h;
					t1 = t;
					t2 = t;
					kol_e++;
					Yi++;
				}
				if(e > e2) {
					System.out.println("e > e2");
					h = h / 2;
					
				}
				if(e1 <= e && e <= e2) {
					System.out.println("e1 < e < e2");

					vect_y = vect_y2.clone();
					Y[Yi] = vect_y.clone();
					t = t + h;
					t1 = t;
					t2 = t;
					kol_e++;
					Yi++;
				}
				//Thread.sleep(500);
			}
		}
		catch(Exception x) {
		}
		return Y;		
	}
	
	private static double[] rk_one_step(double t, double h, double[] start_y, double[] alpha, double[][] beta, double[] p, int q, int n, Method m) {
		double[][] k = new double[q][n];
		double[] vect_temp = new double[n];
		double[] vect_y = new double[n];
		
		try {
			Class c = m.getDeclaringClass();
		    Object ob = c.newInstance();
		    m.setAccessible(true);
		    //������� k1
			k[0] = ((double[]) m.invoke(ob, t + h * alpha[0], start_y)).clone();
			//������� ��� ���������� k �� q-�� k
			for (int j = 1; j < q; j++) {
				double[] sum = new double[n];
				for(int kol = 0 ; kol < j; kol++) {
					 sum = ad(sum, mul(beta[j][kol], k[kol]));
				}
				vect_temp = ad(vect_y, mul(h, sum));
				k[j] = ((double[]) m.invoke(ob, t + alpha[j] * h, vect_temp)).clone();
			}
	
			// ������� Yn+1 � ������� Y � k1, ... , kq
			for (int i = 0; i < n; i++) {
				double[] sum = new double[n];
				for(int j = 0; j < q; j++) {
					sum[i] += k[j][i] * p[j];
				}
				vect_y[i] = vect_y[i] + h * sum[i];
			}
			//t = t + h;
		}
		catch(Exception x) {
		}
		return vect_y;
	}
	
	//������� ��� �������� ������� 10
	private static double[] f10(double t, double[] vect_y) {
		double[] otvet = new double[2];
		otvet[0] = (vect_y[0] / (2.0 + 2.0 * t)) - (2.0 * t * vect_y[1]);
		otvet[1] = (vect_y[1] / (2.0 + 2.0 * t)) + (2.0 * t * vect_y[0]);
		return otvet;
	}
	
	//������� ������� 5
	private static double[] f5(double t, double[] vect_y) {
		double[] otvet = new double[2];
		otvet[0] = -vect_y[1] + t * t + 6 * t + 1; 
		otvet[1] = vect_y[0] - 3 * t * t + 3 * t + 1;
		/*
		 * otvet[0] = -vect_y[1] + t * t + 6 * t + 1; 
otvet[1] = vect_y[0] - 3 * t * t + 3 * t + 1;
		 */
		return otvet;
	}
	
	//������������� ������� ��������� ������� ��������
	private static double[][] toch_znch(double[]ab, int N, int n, Method m) {
		try {
			Class c = m.getDeclaringClass();
		    Object ob = c.newInstance();
		    m.setAccessible(true);
		    return (double[][]) m.invoke(ob, ab, N, n);
		}
		catch(Exception x) {
			System.out.print( x.getMessage() );
		}
		return null;
	}
	
	// ������ �������� ������� �������10 (��� ��������)
	private static double[][] toch_znch10(double[]ab, int N, int n) {
		double[][] Yt = new double[N+1][n+1];
		double h = (ab[1] - ab[0]) / N;
		double t = ab[0];
		for(int i = 0; i <= N; i++) {
			double[] otvet = new double[3];
			otvet[0] = Math.cos(t * t) * Math.sqrt(1 + t);
			otvet[1] = Math.sin(t * t) * Math.sqrt(1 + t);
			otvet[2] = r(t,2);
			t = t + h;
			Yt[i] = otvet;
			//System.out.println("t = " + Yt.get(i)[2] + "\t" +Yt.get(i)[0] + "\t" + Yt.get(i)[1]);
		}
		return Yt;
	}
	
	// ������ �������� ������� �������5 (��� ��������)
	private static double[][] toch_znch5(double[]ab, int N, int n) {
		double[][] Yt = new double[N+1][n+1];
		double h = (ab[1] - ab[0]) / N;
		double t = ab[0];
		for(int i = 0; i <= N; i++) {
			double[] otvet = new double[3];
			otvet[0] = 3 * t * t - t - 1 + Math.cos(t) + Math.sin(t);
			otvet[1] = t * t + 2 - Math.cos(t) + Math.sin(t);
			otvet[2] = r(t,2);
			t = t + h;
			Yt[i] = otvet;
			//System.out.println("t = " + Yt.get(i)[2] + "\t" +Yt.get(i)[0] + "\t" + Yt.get(i)[1]);
		}
		return Yt;
	}
	
	//�������� ��������� ������
	private static boolean isFinish(double t, double b) {
		return (t > b);
	}

	//��������������� �������
	//����� ����������� � �������
	private static void printResults(double[][] R, double[][] T, double Phi0, int N) throws IOException {
		//System.out.println("������ � = " + T.length);
		System.out.println("������ �� = " + R.length);
		System.out.println("======================================");
		//System.out.println("� \t �����E y1 \t\t �����-����� �1 \t �����E y2 \t\t �����-����� �2");
		System.out.println("======================================");
		double t = Phi0;
		double h = ((Math.PI - Phi0) - Phi0) / N;
		for(int i = 0; i < R.length; i++){
			//System.out.println(T[i][2] + "\t" + T[i][0] + "\t" + R[i][0] + "\t" + T[i][1] + "\t" + R[i][1]);
			System.out.println(R[i][0] + "\t" + R[i][1] + "\t" + R[i][2]);
			bufferedWriter.append(R[i][0] + "\t" + R[i][1] + "\t" + R[i][2] + "\t" + t + "\n");
			t += h;			
		}
		//System.out.println("� \t �����E y1 \t\t �����-����� �1 \t �����E y2 \t\t �����-����� �2");
		System.out.println("======================================");
		bufferedWriter.close();
	}
	
	//����� ������������ � �������
	private static void printRazn(double[][] R, double[][] T) {
		System.out.println("������ � = " + T.length);
		System.out.println("������ �� = " + R.length);
		System.out.println("======================================");
		System.out.println("�����������");
		System.out.println("� \t|�����E y1 - �����-����� �1| \t|�����E y2 - �����-����� �2|");
		for(int i = 0; i < T.length; i++){
			System.out.println(T[i][2] + "\t" + Math.abs(T[i][0] - R[i][0]) + "\t\t" + Math.abs(T[i][1] - R[i][1]));
		}
		System.out.println("� \t|�����E y1 - �����-����� �1| \t|�����E y2 - �����-����� �2|");
	}
	
	//���������� ������������ �����������
	private static double[] maxRazn(double[][] R, double[][] T, int n) {
		double[] raznMax = new double[n];
		for(int i = 0; i < T.length; i++)
			for(int j = 0; j < n; j++){
				if(raznMax[j] < Math.abs(T[i][j] - R[i][j])) {
					raznMax[j] = Math.abs(T[i][j] - R[i][j]);
				}
			}
		return raznMax;
	}
	
	//���������� ������������ ������������
	private static void allMaxRazn(double[] ab, double[] alpha, double[][] beta,
			double[] p, int q, int n,double[] ua, int startN, String f, String t, int k) {
		//int k = 100;
		int N = startN;

		double[][] R, T;
		R = new double[startN+1][n];
		T = new double[startN+1][n+1];
		
		Method[] allMethods = Rynge.class.getDeclaredMethods();
		for(int i = 0; i < k; i++) {
			for (Method m : allMethods)
			    if (m.getName().equals(f)) {
			    	R = rk(ab,alpha, beta, p, q, n, N, ua, m);
			    	break;
			    }
			
			for (Method m : allMethods)
			    if (m.getName().equals(t)) {
			    	T = toch_znch(ab, N, n, m);
			    	break;
			    }
			String text = String.valueOf(N + "\t");
			for(int j = 0; j < n; j++){
				text += String.valueOf(maxRazn(R, T, n)[j]) + "\t";
			}
			System.out.println(text);
			N += 100;
		}
		
	}
	
	//����������
	private static double r(double value, int k){
        return (double)Math.round((Math.pow(10, k)*value))/Math.pow(10, k);
	}

	//�������� ��������� �� ������
	private static double[] mul(double a, double[] b) {
		double[] c = new double[b.length];
		for(int i = 0; i < b.length; i++) {
			c[i] = b[i] * a;
		}
		return c;
	}
	
	//�������� ��������
	private static double[] ad(double[] a, double[] b) {
		double[] c = new double[b.length];
		for(int i = 0; i < b.length; i++) {
			c[i] = b[i] + a[i];
		}
		return c;
	}
	
}
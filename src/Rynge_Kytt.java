import java.util.ArrayList;

public class Rynge_Kytt {

	public static void main(String[] args) {
		System.out.println("��");
		System.out.println("======================================");
		double[] ab = {0, 2};
		int N = 200;
		double[] alpha = {0, 1};
		double[][] beta = {{0, 0}, {1, 0}};
		double[] p = { 0.5 , 0.5};
		int q = 2;
		int n = 2;
		double h = 0.10;
		double[] ua = {Math.cos(0 * 0) * Math.sqrt(1 + 0),
				Math.sin(0 * 0) * Math.sqrt(1 + 0)};
		ua[0] = 1;
		ua[1] = 0;
		for(int i = 0; i < toch_znch(ab, N).size(); i++) {
			System.out.println("t = " + toch_znch(ab, N).get(i)[2] + "\t" +toch_znch(ab, N).get(i)[0] + "\t" + toch_znch(ab, N).get(i)[1]);
		}
		System.out.println("==========================================");
		rk(ab,alpha, beta, p, q, n, N, ua);
/*		
		for(int i = 0; i < rk(ab,alpha, beta, p, q, n, N, ua).size(); i++) {
			System.out.println("\t" +rk(ab,alpha, beta, p, q, n, N, ua).get(i)[0] + "\t" + rk(ab,alpha, beta, p, q, n, N, ua).get(i)[1]);
		}
*/		
		
	}
	
	private static ArrayList<double[]> rk(double[] ab, double[] alpha, double[][] beta,
			double[] p, int q, int n, int N, double[] ua) {//, double[] vect_curY) {
		ArrayList<double[]> Y = new ArrayList<>();
		double[] vect_y = new double[n];
		vect_y = ua;
		double[] vect_temp = new double[n];
		double t = ab[0];
		double[][] k = new double[q][n];
		double h = (ab[1] - ab[0]) / N;
		while (!Finish(t, ab[1])) {
			k[0] = f(t + h * alpha[0], vect_y);
						
			for (int j = 1; j < q; j++) {
				double[] sum = new double[n];
				for(int kol = 0 ; kol < j; kol++) {
					 sum = ad(sum, mul(beta[j][kol], k[kol]));
				}
				vect_temp = ad(vect_y, mul(h, sum));			
				k[j] = f(t + alpha[j] * h, vect_temp);
			}
			
			vect_y = ad(vect_y, mul(h, ad(mul(p[0], k[0]), mul(p[1], k[1]))));
			System.out.println(r(t,2) + "\t" + vect_y[0] + "\t" + vect_y[1]);
			t = t + h;
			
			Y.add(vect_y);
		}

		return Y;
	}

	private static double[] f(double t, double[] vect_y) {
		double[] otvet = new double[2];
		otvet[0] = (vect_y[0] / (2.0 + 2.0 * t)) - (2.0 * t * vect_y[1]);
		otvet[1] = (vect_y[1] / (2.0 + 2.0 * t)) + (2.0 * t * vect_y[0]);
		
		return otvet;
	}

	// ������
	private static ArrayList<double[]> toch_znch(double[]ab, int N) {
		ArrayList<double[]> Yt = new ArrayList<>();
		double h = (ab[1] - ab[0]) / N;
		double t = ab[0];
		for(int i = 0; i <= N; i++) {
			double[] otvet = new double[3];
			otvet[0] = Math.cos(t * t) * Math.sqrt(1 + t);
			otvet[1] = Math.sin(t * t) * Math.sqrt(1 + t);
			otvet[2] = r(t,2);
			t = t + h;
			Yt.add(otvet);
		}
		return Yt;
	}

	private static boolean Finish(double t, double b) {
		return (t >= b);
	}

	private static double[] mul(double a, double[] b) {
		double[] c = new double[b.length];
		for(int i = 0; i < b.length; i++) {
			c[i] = b[i] * a;
		}
		return c;
	}
	
	private static double[] ad(double[] a, double[] b) {
		double[] c = new double[b.length];
		for(int i = 0; i < b.length; i++) {
			c[i] = b[i] + a[i];
		}
		return c;
	}
	
	private static double r(double value, int k){
        return (double)Math.round((Math.pow(10, k)*value))/Math.pow(10, k);
}
}
package gui;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

/**
 * SimpleComponent
 *
 * @author Eugene Matyushkin aka Skipy
 * @since 21.10.2010
 */
public class SimpleComponent extends JPanel {

	RK_FOR_GUI rk;
	
    public SimpleComponent() throws IOException {
        setOpaque(true);
        rk = new RK_FOR_GUI();
    }

    @Override
    protected void paintComponent(Graphics g) {
        //super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
        
        //g2d.setClip(0,0, getWidth(), getHeight() * 2);
        //g2d.setClip(getWidth() / 4, getHeight() / 4, getWidth() / 2, getHeight() / 2);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setColor(Color.BLUE);
        
        g2d.drawLine(50, 500, 750, 500);//OX
        g2d.drawLine(50, 50, 50, 500);//OY
        
        g2d.setColor(Color.BLACK);
        
        ArrayList<double[]> RK = new ArrayList<>();
        try {
			RK = rk.getRK();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        int startX = 50;
        int startY = 250;
        int[] xPoints = new int[RK.size()];
        int[] yPoints = new int[RK.size()];
        for(int i = 0; i < RK.size(); i++) {
        	xPoints[i] = startX + (int)(RK.get(i)[0] * 1000);
        	yPoints[i] = startY - (int)(RK.get(i)[1] * 200);
        	System.out.println("x = " + xPoints[i] + "\ty = " + yPoints[i]);
        }
        g2d.drawPolyline(xPoints, yPoints, RK.size());
        /*
        g2d.fillOval(10, 10, getWidth() - 20, getHeight() * 2 - 20);
        g2d.setColor(Color.red);
        g2d.fillOval(20, 20, getWidth() - 40, getHeight() - 40);
        g2d.setColor(Color.yellow);
        g2d.fillOval(30, 30, getWidth() - 60, getHeight() - 60);
        g2d.setColor(Color.black);
        g2d.fillOval(getWidth()/4 - getWidth()/16, getHeight()/2-getHeight()/8, getWidth()/8, getHeight()/8);
        g2d.fillOval(getWidth()*3/4 - getWidth()/16, getHeight()/2-getHeight()/8, getWidth()/8, getHeight()/8);
        g2d.setStroke(new BasicStroke(10, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        g2d.drawArc(getWidth()/4, getHeight()/4, getWidth()/2, getHeight()/2, 225, 90);
        */
        
    }
}
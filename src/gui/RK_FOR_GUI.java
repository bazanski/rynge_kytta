package gui;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class RK_FOR_GUI {

	final static double e1 = 0.0001;// * Math.pow(10, -330);
	final static double e2 = 0.01;
	
	
	static double[] alpha = {0, 1};
	static double[][] beta = {{0, 0}
					, {1, 0}};
	static double[] p = { 0.5 , 0.5};
	static double R0 = 0;
	static double Z0 = 0.4;
	static double Phi0 = 1.2;
	static double[] ua = {R0, -Z0, Phi0};
	//double[] ab = {0, 2};
	static double[] ab = {0, Math.PI - Phi0};
	static int q = 2;
	static int n = 3;
	static int N = 20000;
	
	public RK_FOR_GUI() throws IOException {
		
		//dinamic_rk_new_final(ab,alpha, beta, p, q, n, N, ua);
		
	}
	
	public static ArrayList<double[]> getRK() throws IOException {
		return dinamic_rk_new_final(ab,alpha, beta, p, q, n, N, ua);
	}
	
	private static ArrayList<double[]> dinamic_rk_new_final(double[] ab, double[] alpha, double[][] beta,
			double[] p, int q, int n, int N, double[] ua) throws IOException {
        ArrayList<double[]> allY = new ArrayList<double[]>();
        double[] y = ua.clone();
        double[] y0 = ua.clone();
        double[] y1 = ua.clone();
        double[] y2 = ua.clone();
        double t = ab[0],
        h = 1e-7;

        allY.add(ua.clone());

	    System.out.println("e \t R \t Z \t PHI \t h \t t");
		System.out.println(0 + "\t" + y[0] + "\t" + y[1] + "\t" + y[2] + "\t" + h + "\t" + t);
	    while( !isFinal(y[2]) ) {
	
	        y = one_step_final( t, h, y0, alpha, beta, p, q, n ).clone();
	        y1 = one_step_final( t, h/2, y0, alpha, beta, p, q, n ).clone();
	        y2 = one_step_final( t+h/2, h/2, y1, alpha, beta, p, q, n ).clone();
	
	        double[] oE = new double[n];
	        double e = 0;
	        int count_y = n, count_k = q;
	        for( int number_y = 0; number_y < count_y; number_y++ ) {
	            oE[number_y] = Math.abs((y1[number_y] - y2[number_y]) / (Math.pow(2, count_k - 1) - 1));
	            e += oE[number_y];
	        }
	
	        e = e/count_y;
	        if ( e < e1 ) {
	        	allY.add(y.clone());
	            y0 = y.clone();
	            t += h;
	            h = h * 2;
	        }
	
	        if ( e > e2 ) {
	            h = h / 2;
	        }
	
	        if( e1 <= e && e <= e2 ) {
	        	allY.add(y.clone());
	            y0 = y2.clone();
	            t += h;
	        }
	        System.out.println(e + "\t" + y[0] + "\t" + y[1] + "\t" + y[2] + "\t" + h + "\t" + t);
	    }
	    return allY;
	}

	private static double[] one_step_final(double t, double h, double[] start_y, double[] alpha, double[][] beta, double[] p, int q, int n) {
		double[][] array_k = new double[q][n];
		double[] yTemp = {0, 0, 0},
		           y = start_y.clone();
		array_k[0][0] = 0;
		array_k[0][1] = 0;
		array_k[0][2] = 0;

		array_k[0] = f10_final( t + h*alpha[0], start_y);

		int count_k = q, count_y = n;
		for ( int number_k = 1; number_k < count_k; number_k++ ) {
	           //����������  ��������� y � k=f(t,y)
	           for ( int number_y = 0; number_y < count_y; number_y++ ) {
	               // ����( beta*k )
	               double sumBeta_K = 0;
	               for ( int j = 0; j < number_k; j++ ) {
	                   sumBeta_K += array_k[j][number_y]*beta[number_k][j];
	               };
	               yTemp[number_y] = start_y[number_y] + h * sumBeta_K;
	           };

	           array_k[number_k] = f10_final( t + h*alpha[number_k], yTemp ).clone();
	       };

	       double[] sumP_K = {0,0,0};
	       for ( int number_y = 0; number_y < count_y; number_y++ ) {
	           for ( int number_k = 0; number_k < count_k; number_k++ ) {
	               sumP_K[number_y] += p[number_k]*array_k[number_k][number_y];
	           };
	           y[number_y] += h*sumP_K[number_y];
	       };
	       return y;      
	}
	
	private static boolean isFinal(double Phi) {
		if(Phi <= Math.PI - Phi0)// && Math.abs(Phi) < 15)
			return false;
		else
			return true;
	}
	
	private static double[] f10_final(double t, double[] vect_y) {
		double alpha_sqr = 0.39 * 0.39;
		double k = 0;
		double r = vect_y[0], Z = vect_y[1], phi = vect_y[2];
		if(Math.abs(t) < 1e-4) {
			k = - (Z / (2 * alpha_sqr)); 
		}
		else {
			k = (-Math.sin(phi) / r);
		}		
		
		double[] otvet = new double[3];
		otvet[0] = Math.cos(phi);						// r"
		otvet[1] = -Math.sin(phi);						// Z"
		otvet[2] =  k - (Z / alpha_sqr);			//phi"
		
		return otvet;
	}
	
}
